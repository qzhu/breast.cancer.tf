#!/usr/bin/python
import sys
import re
import os
import numpy
np = numpy
from scipy.stats import hypergeom
import reader
rp = reader

def read_pval(n, valid):
	f = open(n)
	p = []
	tp, tn = [], []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		magnitude = float(ll[-2])
		p.append(magnitude)
		if magnitude > 0:
			tp.append(magnitude)
		else:
			tn.append(magnitude)
	f.close()
	nn1 = np.array(tp)
	nn2 = np.array(tn)
	uq = np.percentile(nn1, 75)
	lq = np.percentile(nn2, 25)

	f = open(n)
	m = []
	valid_set = set(valid)
	for l in f:
		l = l.rstrip("\n").split()
		if not l[0] in valid_set:
			continue
		pval = float(l[1])
		#effect = abs(float(l[4]))
		effect = float(l[4])
		qval = float(l[5])
		if effect < 0:
			#effect = -1.0 * effect / lq
			effect = effect / lq
		elif effect > 0:
			effect = effect / uq
		#if qval>0.001:
		if qval>0.1:
			m.append((l[0], 0))
		else:
			m.append((l[0], effect))
		
		#m.append((l[0], effect))

		'''
		if qval<=0.01 and effect>1:
		#if qval<=0.001:
		#if qval<=0.01:
		#if qval<=0.001:
			effect = 1
			#effect = 1.0 * effect
		else:
			effect = 0
		'''
	f.close()
	m.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank = {}
	for i,(j,k) in enumerate(m):
		rank[j] = i
	return m, rank

if __name__=="__main__":
	valid_genes = rp.read_genes("gene_entrez_symbol.txt", sep="\t", field=1)
	tfs = rp.read_tfs("human.TF.list.in.Seek.txt")

	choices = set(["lumA", "lumB", "basal", "her2"])

	param1 = {"her2":"erbb2", "lumA":"lumA", "lumB":"lumB.NEW", "basal":"basal"}
	param2 = {"her2":"Her2", "lumA":"Luminal_A", "lumB":"Luminal_B", "basal":"basal_like"}
	param3 = {"her2":"her2", "lumA":"lumA", "lumB":"lumB", "basal":"basal"}

	path = {"meth":"methyl.Thomas/methylation_"}

	subtypes = ["lumA", "basal", "her2", "lumB"]
	dereg = ["meth"]
	cutoff = 0.5
	pvals = {}
	for s in subtypes:	
		#coexp = rp.read_genes("/home/qzhu/ENCODE/motifs.good/%s" % s, sep=" ", field=0)
		#all coexpressed genes
		coexp = rp.read_genes("enrich/coexpressed/%s" % s, sep=" ", field=0)

		#targets
		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.0.10" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.all.exp.0.10" % s, sep=" ", field=0)

		#motif-derived coexpressed TFs
		downs = rp.read_tfs("enrich/downstream/%s" % s)

		#just coexpressed TFs
		#downs = rp.read_tfs("/home/qzhu/enrich/coexpressed/tf/%s" % s)

		#downs_nonspecific = rp.read_tfs("/home/qzhu/enrich/downstream/%s.nonspecific" % s)
		#downs = set(downs) | set(downs_nonspecific)
		ups = rp.read_tfs("enrich/upstream/%s" % s)
		#ups = set(ups) - set(downs)
		pvals.setdefault(s, {})
		for d in dereg:
			test_file = path["meth"] + param2[s] + "_difference_t_test.qval"
			pval, rank = read_pval(test_file, valid_genes)
			ret = rp.do_scenario(ups_tf=ups, downs_tf=downs, coexp_gene=coexp, tfs=tfs, \
				rank=rank, pval=pval, cutoff=cutoff)
			pvals[s].setdefault(d, {})
			pvals[s][d]["up"] = ret["up"]["pval"]
			pvals[s][d]["down"] = ret["down"]["pval"]
			pvals[s][d]["coexp"] = ret["coexp"]["pval"]
			print s, d, ret["up"]["pval"], ret["down"]["pval"], ret["coexp"]["pval"]
	
	'''
	pval, rank = read_pval(test_file, valid_genes)
	#ups = read_dereg_table("/home/qzhu/ICGC.breast.cancer/table/upstream.%s.txt" % param3[cc])
	#downs = read_dereg_table("/home/qzhu/ICGC.breast.cancer/table/coexp.%s.txt" % param3[cc])
	coexp = read_tfs("/tmp/%s.2" % param1[cc])
	#coexp = read_tfs("/media/storage1/qzhu/ENCODE.AWG/%s.num.region.prioritized" % param3[cc]) #from Chip-seq (GOOD)
	ups = read_tfs("/home/qzhu/genomatix.test/coexpressed/tf/%s.early.good" % param3[cc])
	downs = read_tfs("/home/qzhu/genomatix.test/coexpressed/tf/%s.good" % param3[cc])
	coexp = set(coexp) - set(ups) - set(downs)
	'''
	'''
	#filter by those in pval
	ups_new = [a for a in ups if rank.has_key(a)]
	downs_new = [a for a in downs if rank.has_key(a)]
	ups = ups_new
	downs = downs_new	
	set_coexp = [a for a in coexp if rank.has_key(a)]

	set_ups = set(ups)
	set_downs = set(downs)

	#get tfs
	pval_tfs = []
	rank_tfs = {}
	for t in tfs:
		if rank.has_key(t):
			pval_tfs.append(pval[rank[t]])
	pval_tfs.sort(lambda x,y:cmp(x[1], y[1]), reverse=True)
	for i,(j,k) in enumerate(pval_tfs):
		rank_tfs[j] = i

	prob_down = test(pval, rank, set_downs)
	prob_up = test(pval, rank, set_ups)

	prob_tfs_down = test(pval_tfs, rank_tfs, set_downs)
	prob_tfs_up = test(pval_tfs, rank_tfs, set_ups)

	prob_coexp = test(pval, rank, set_coexp)

	print "P-value for downstream (all genes):", prob_down
	print "P-value for upstream (all genes):", prob_up
	print "P-value for downstream (Tfs):", prob_tfs_down
	print "P-value for upstream (Tfs):", prob_tfs_up
	print "P-value for coexpressed targets:", prob_coexp
	'''
