#!/usr/bin/python
import sys
import re
import os
import numpy
np = numpy
from scipy.stats import hypergeom
import reader
rp = reader

def read_pval(n, valid, subtype):
	f = open(n)
	mm = {}
	tp,tn = [],[]
	valid_set = set(valid)
	while True:
		l = f.readline()
		if l=="": break
		l = l.rstrip("\n")
		tit = l
		sub = tit.split(" ", 2)[-1]
		genes = f.readline().rstrip("\n").split()
		if sub!=subtype:
			continue
		for t in genes:
			g1,g2 = t.split(":")
			if not g1 in valid_set: continue
			mm.setdefault(g1, [])
			magnitude = float(g2)
			if magnitude > 0:
				tp.append(magnitude)
			else:
				tn.append(magnitude)
			mm[g1].append(magnitude)
	f.close()

	nn1 = np.array(tp)
	nn2 = np.array(tn)
	uq = np.percentile(nn1, 75)
	lq = np.percentile(nn2, 25)
	m = []
	for k in mm.keys():
		tt = []
		for tx in mm[k]:
			if tx < 0:
				tx = tx / lq
			else:
				tx = tx / uq
			tt.append(tx)
		ttd = max(tt)
		m.append((k, ttd))
	m.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank = {}
	for i,(j,k) in enumerate(m):
		rank[j] = i

	return m, rank

if __name__=="__main__":
	valid_genes = rp.read_genes("gene_entrez_symbol.txt", sep="\t", field=1)
	tfs = rp.read_tfs("human.TF.list.in.Seek.txt")

	choices = set(["lumA", "lumB", "basal", "her2"])

	param1 = {"her2":"erbb2", "lumA":"lumA", "lumB":"lumB.NEW", "basal":"basal"}
	param2 = {"her2":"Her2-enriched", "lumA":"Luminal A", "lumB":"Luminal B", "basal":"Basal-like"}
	param3 = {"her2":"her2", "lumA":"lumA", "lumB":"lumB", "basal":"basal"}

	test_file = "cna.Metabric/all_cna_curtis_gene_full_value"
	
	#subtypes = ["lumA", "basal", "lumB", "her2"]
	subtypes = ["lumA", "basal"]
	dereg = ["cna"]
	cutoff = 0.5
	pvals = {}

	for s in subtypes:
		#coexp = rp.read_genes("/home/qzhu/ENCODE/motifs.good/%s" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.all.exp.0.10" % s, sep=" ", field=0)
		coexp = rp.read_genes("seq.3/result.%s.0.10" % s, sep=" ", field=0)
		downs = rp.read_tfs("downstream/%s" % s)
		#downs_nonspecific = rp.read_tfs("/home/qzhu/enrich/downstream/%s.nonspecific" % s)
		#downs = set(downs) | set(downs_nonspecific)
		ups = rp.read_tfs("upstream/%s" % s)
		#ups = set(ups) - set(downs)
		combined = rp.read_tfs("combined/%s" % s)
		pvals.setdefault(s, {})
		for d in dereg:
			pval, rank = read_pval(test_file, valid_genes, param2[s])
			ret = rp.do_scenario(ups_tf=combined, downs_tf=combined, coexp_gene=coexp, tfs=tfs, \
				rank=rank, pval=pval, cutoff=cutoff)
			pvals[s].setdefault(d, {})
			pvals[s][d]["up"] = ret["up"]["pval"]
			pvals[s][d]["down"] = ret["down"]["pval"]
			pvals[s][d]["coexp"] = ret["coexp"]["pval"]
			print s, d, ret["up"]["pval"], ret["down"]["pval"], ret["coexp"]["pval"]
	
