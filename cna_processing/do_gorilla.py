#!/usr/bin/python
import sys
import re
import os
import numpy
np = numpy
from scipy.stats import hypergeom

def read_pval(n, valid):
	f = open(n)
	p = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		magnitude = float(ll[-2])
		p.append(magnitude)
	f.close()
	nn = np.array(p)
	uq = np.percentile(nn, 75)
	lq = np.percentile(nn, 25)

	f = open(n)
	m = []
	valid_set = set(valid)
	for l in f:
		l = l.rstrip("\n").split()
		if not l[0] in valid_set:
			continue
		pval = float(l[1])
		effect = abs(float(l[4]))
		#effect = float(l[4])
		qval = float(l[5])
		
		'''
		if effect < 0:
			#effect = -1.0 * effect / lq
			effect = effect / lq
		elif effect > 0:
			effect = effect / uq
	
		'''

		
		if qval>0.01:
			m.append((l[0], 0))
		else:
			m.append((l[0], effect))
		
		#m.append((l[0], effect))

		'''
		if qval<=0.01 and effect>1:
		#if qval<=0.001:
		#if qval<=0.01:
		#if qval<=0.001:
			effect = 1
			#effect = 1.0 * effect
		else:
			effect = 0
		'''
	f.close()
	m.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank = {}
	for i,(j,k) in enumerate(m):
		rank[j] = i

	return m, rank

def read_dereg_table(n):
	f = open(n)
	m = []
	f.readline()
	while True:
		l = f.readline()
		if l=="": break
		l = l.rstrip("\n").split("\t")
		m.append(l[0])
	f.close()
	return m

def read_tfs(n):
	f = open(n)
	m = []
	for l in f:
		l = l.rstrip("\n")
		m.append(l)
	f.close()
	return m

def read_genes(n):
	f = open(n)
	genes = []
	for l in f:
		l = l.rstrip("\n")
		genes.append(l.split("\t")[1])
	f.close()
	return genes

def test(pval, rank, test_group):
	#n is total relevant
	#k is total relevant within query
	#M is total items
	#N is total items within query
	M = len(rank.keys())
	n = len(test_group)

	ov = 0
	tt = []
	for N in range(1, len(pval)):
		this_item = pval[N-1][0]
		if this_item in test_group:
			ov+=1
		prob = 1 - hypergeom.cdf(ov, M, n, N) + hypergeom.pmf(ov, M, n, N)
		tt.append((ov, M, n, N, prob))
	
	tt.sort(lambda x,y:cmp(x[4], y[4]))

	return tt[0]

if __name__=="__main__":
	valid_genes = read_genes("/home/qzhu/gene_entrez_symbol.txt")
	tfs = read_tfs("/home/qzhu/human.TF.list.in.Seek.txt")
	
	pval, rank = read_pval("/media/storage1/qzhu/CNA_Meth_Validation/cna_processing/cna_luminal_B_difference_t_test.qval", valid_genes)
	#pval, rank = read_pval("/home/qzhu/ICGC.breast.cancer/Methylation/methylation_luminal_B_difference.t.test.qval", valid_genes)
	#pval, rank = read_pval("/home/qzhu/ICGC.breast.cancer/CNV/cna_luminal_A_difference.t.test.qval", valid_genes)
	ups = read_dereg_table("/home/qzhu/ICGC.breast.cancer/table/upstream.lumB.txt")
	downs = read_dereg_table("/home/qzhu/ICGC.breast.cancer/table/coexp.lumB.txt")

	#filter by those in pval
	ups_new = [a for a in ups if rank.has_key(a)]
	downs_new = [a for a in downs if rank.has_key(a)]
	ups = ups_new
	downs = downs_new	

	set_ups = set(ups)
	set_downs = set(downs)

	#get tfs
	pval_tfs = []
	rank_tfs = {}
	for t in tfs:
		if rank.has_key(t):
			pval_tfs.append(pval[rank[t]])
	pval_tfs.sort(lambda x,y:cmp(x[1], y[1]), reverse=True)
	for i,(j,k) in enumerate(pval_tfs):
		rank_tfs[j] = i

	prob_down = test(pval, rank, set_downs)
	prob_up = test(pval, rank, set_ups)

	prob_tfs_down = test(pval_tfs, rank_tfs, set_downs)
	prob_tfs_up = test(pval_tfs, rank_tfs, set_ups)

	print "P-value for downstream (all genes):", prob_down
	print "P-value for upstream (all genes):", prob_up
	print "P-value for downstream (Tfs):", prob_tfs_down
	print "P-value for upstream (Tfs):", prob_tfs_up
