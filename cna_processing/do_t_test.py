#!/usr/bin/python
import sys
import numpy
import scipy
from scipy import stats
np = numpy

if __name__=="__main__":
	f = open("Tumor.2.map")
	ph = {}
	sam = []
	for l in f:
		l = l.rstrip("\n").split("\t")
		ph.setdefault(l[1], set([]))
		ph[l[1]].add(l[0])
		sam.append(l[0])
	f.close()

	f = open(sys.argv[1])
	m = []
	m_sam = []
	m_map = {}
	for l in f:
		lx = l.rstrip("\n")
		lxt = l.rstrip("\n").split("/")[1]
		m.append(lx)
		m_sam.append(lxt)
		m_map[lxt] = lx
	f.close()

	#p = "Luminal A"
	p = sys.argv[2]
	tcga = ph[p]
	sx = list(set(tcga) & set(m_sam)) #these are the samples for the subtype
	num_p = len(sx) #number of samples

	gene = {}
	for ml in sx:
		f = open(m_map[ml])
		lx = f.readline().rstrip("\n").split()
		for x in lx:
			(i,j) = x.split(":")
			gene.setdefault(i, [])
			gene[i].append(float(j))
		f.close()

	all_genes = set(gene.keys())

	for g in all_genes:
		if gene.has_key(g):
			needed = num_p - len(gene[g])
		else:
			needed = num_p
		n1 = needed
		for i in range(needed):
			gene.setdefault(g, [])
			gene[g].append(0)
		
		if n1!=0: continue
		a1 = np.array(gene[g])
		(tstat, pval) = stats.ttest_1samp(a1, 0)
		print g, pval, num_p, 0, np.average(a1)
