# Illustration of the CNA processing pipeline

We use a CNA dataset generated from the Perou lab.

Running the scripts require Python 2.7 in a Unix/Linux environment.

### Input files:
`CNA_Perou_output/*` which contains the CNA segmentations 

### Step 1:
To start processing:

run the `./do_one.2.sh` script,

this will align each CNA segment to hg19 assembly in order to locate gene position for CNA segments.
then it will map them to genes using the `summarize_2.py` script.

the output is in `summarized_2/*`, gene-based CNA values.

### Step 2:
Then run `do_t_test.py`

This will perform a 1-sample t-test to find significantly associated CNA genes.

The output is in the files: `cna_luminal_A_difference_t_test`, `cna_basal_like_difference_t_test`, etc.

### Step 3:
Then run `get_q_value.py`

This will perform Q-value procedure for multiple hypothesis testing.

The output is in the file `cna_luminal_A_difference_t_test.qval`, `cna_basal_like_difference_t_test.qval`

### Step 4:
Then run `build_gene_set_t_test_2.py`

This will build a gene-by-CNA table for visualization.

