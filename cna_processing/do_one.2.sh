source ~/.init
reffile=hg19.gene.start.end.sorted.bed
sort-bed CNA_Perou_output_bed/$1|bedops -e -1 $reffile - > /tmp/$1.1
sort-bed CNA_Perou_output_bed/$1|bedops -e -1 - $reffile > /tmp/$1.2
bedops -u /tmp/$1.1 /tmp/$1.2 > /tmp/$1.12
./summarize_2.py /tmp/$1.12 0 4 > summarized_2/$1
rm -rf /tmp/$1.12
rm -rf /tmp/$1.1
rm -rf /tmp/$1.2
