#!/usr/bin/python

import sys
import os
import numpy
np = numpy
def read_gene_set(n):
	f = open(n)
	p = []
	p_pos = []
	p_neg = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		magnitude = float(ll[-1])
		p.append(magnitude)
		if magnitude > 0: p_pos.append(magnitude)
		elif magnitude < 0: p_neg.append(magnitude)
	f.close()
	nn = np.array(p)
	uq = np.percentile(np.array(p_pos), 75)
	lq = np.percentile(np.array(p_neg), 25)
	print lq, uq

	f = open(n)
	gs_pos = set([])
	gs_neg = set([])
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		gene = ll[0]
		pval = float(ll[1])
		magnitude = float(ll[-1])
		if pval<1e-2:
		#if pval<=1:
			#if magnitude<lq*3:
			if magnitude<0:
				if "|" in gene:
					gg = gene.split("|")
					for gene in gg:
						gs_neg.add((gene, magnitude / lq))
				else:
					gs_neg.add((gene, magnitude / lq))
			#elif magnitude>uq*3:
			elif magnitude>0:
				if "|" in gene:
					gg = gene.split("|")
					for gene in gg:
						gs_pos.add((gene, magnitude / uq))
				else:
					gs_pos.add((gene, magnitude / uq))
	f.close()
	return gs_neg, gs_pos

basal_neg, basal_pos = read_gene_set("cna_basal_like_difference_t_test")
lumA_neg, lumA_pos = read_gene_set("cna_luminal_A_difference_t_test")
lumB_neg, lumB_pos = read_gene_set("cna_luminal_B_difference_t_test")
her2_neg, her2_pos = read_gene_set("cna_Her2_difference_t_test")


print len(basal_neg), len(basal_pos)
print len(lumA_neg), len(lumA_pos)
print len(lumB_neg), len(lumB_pos)
print len(her2_neg), len(her2_pos)


sys.stdout.write("CNA Lost Basal-like\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in basal_neg]) + "\n")
sys.stdout.write("CNA Gained Basal-like\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in basal_pos]) + "\n")

sys.stdout.write("CNA Lost Luminal A\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumA_neg]) + "\n")
sys.stdout.write("CNA Gained Luminal A\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumA_pos]) + "\n")

sys.stdout.write("CNA Lost Luminal B\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumB_neg]) + "\n")
sys.stdout.write("CNA Gained Luminal B\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumB_pos]) + "\n")

sys.stdout.write("CNA Lost Her2-enriched\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in her2_neg]) + "\n")
sys.stdout.write("CNA Gained Her2-enriched\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in her2_pos]) + "\n")

