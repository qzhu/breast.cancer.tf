#!/usr/bin/python

import sys
import re

def get_distance_from_tss(s1, s2, t1, t2):
	dist = min(abs(s1 - t1), abs(s1 - t2), abs(s2 - t1), abs(s2 - t2))
	return dist

def get_overlap(s1, s2, t1, t2):
	ov = -1
	if s1 <= t1 and s2 <= t2:
		ov = s2 - t1
	elif s1 <= t1 and s2 > t2:
		ov = t2 - t1
	elif s1 > t1 and s2 <= t2:
		ov = s2 - s1
	elif s1 > t1 and s2 > t2:
		ov = t2 - s1
	return ov

def check_overlap(s1, s2, t1, t2):
	isGood = False
	if s1<=t1 and s2>t2:
		isGood = True
	elif s1>t1 and s2<=t2:
		isGood = True
	elif s1<=t1 and s2<=t2 and t1<=s2:
		isGood = True
	elif t1<=s1 and t2<=s2 and s1<=t2:
		isGood = True
	return isGood


if __name__=="__main__":
	f = open(sys.argv[1])
	#distance_threshold = int(sys.argv[2])

	score_column = 4
	if len(sys.argv)==3:
		score_column = int(sys.argv[2])	

	ml = [] #reference
	mm = [] #user

	for l in f:
		l = l.rstrip("\n")
		if l.endswith("\tREF"):
			ll = l.split("\t")
			gene = ll[3]
			sign = ll[4]
			v_chr, v_start, v_end = ll[0], int(ll[1]), int(ll[2])
			ml.append((v_chr, v_start, v_end, gene, sign))
		else:
			ll = l.split("\t")
			v_chr, v_start, v_end = ll[0], int(ll[1]), int(ll[2])
			if score_column==-1:
				score = 1
			else:
				score = float(ll[score_column])
			mm.append((v_chr, v_start, v_end, score))
	f.close()	

	maps = {}
	frags = {}
	genes = set([])
	j = 0

	for i in range(len(mm)):
		t_chr, t_start, t_end, sc = mm[i] #user fragment

		#if i%100==0:
		#	sys.stdout.write("%d of %d\n" % (i, len(mm)))

		for k in range(j, len(ml)): #reference
			s_chr, s_start, s_end, g, sign = ml[k]
			if t_chr==s_chr and check_overlap(t_start, t_end, s_start, s_end)==True:
				frags.setdefault((t_chr, t_start, t_end, sc), set([]))
				frags[(t_chr, t_start, t_end, sc)].add((g, sign))
				genes.add(g)
				temp_j = k
				#print i, temp_j

	frags_g = {}
	for k in frags.keys():
		sc = k[3]
		for g in frags[k]:
			maps.setdefault(g, 0)
			#maps[g]+=sc/len(frags[k])
			maps[g]+=sc*len(frags[k])
			#maps[g]+=1.0/len(frags[k])
			frags_g.setdefault(g, [])
			frags_g[g].append(len(frags[k]))

	for g in maps.keys():
		maps[g] = maps[g] / float(sum(frags_g[g]))

	maps_it = maps.items()
	maps_it.sort(lambda x,y:cmp(x[1], y[1]), reverse=True)
	#print maps_it
	val = []
	for ((x, sign),y) in maps_it:
		val.append(y)
	uq = int(len(val) * 0.25)

	xt = []
	for ((x, sign),y) in maps_it:
		#k = float(y) / float(val[uq])
		k = float(y)
		xt.append("%s:%.2f" % (x, k))
	sys.stdout.write(" ".join(xt) + "\n")
