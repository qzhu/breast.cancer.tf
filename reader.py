#!/usr/bin/python
import sys
import re
import os
import numpy
np = numpy
from scipy.stats import hypergeom

def read_tfs(n):
	f = open(n)
	m = []
	for l in f:
		l = l.rstrip("\n")
		m.append(l)
	f.close()
	return m

def read_genes(n, sep="\t", field=0):
	f = open(n)
	genes = []
	for l in f:
		l = l.rstrip("\n")
		genes.append(l.split(sep)[field])
	f.close()
	return genes

def read_coexp(n, sep="\t"):
	f = open(n)
	is_query = False
	is_normal = False
	coexp = {}
	pval = {}
	while True:
		l = f.readline()
		if l=="": break
		l = l.rstrip("\n")
		if l.startswith("Query genes:"):
			is_query = True
			is_normal = False
			h = f.readline().rstrip("\n").split("\t")
			continue
		if l.startswith("Coexpressed genes:"):
			is_query = False
			is_normal = True
			h = f.readline().rstrip("\n").split("\t")
			continue
		if is_query:
			tt = l.split("\t")
			ht = dict(zip(h, tt))
			coexp[ht["Query Gene"]] = float(ht["Coexpression Score"])
			pval[ht["Query Gene"]] = float(ht["P-Value"])
		if is_normal:
			tt = l.split("\t")
			ht = dict(zip(h, tt))
			coexp[ht["Gene"]] = float(ht["Coexpression Score"])
			pval[ht["Gene"]] = float(ht["P-Value"])
	f.close()
	return {"coexp.score": coexp, "pval": pval}

def test(pval, rank, test_group, cutoff=0.1):
	#n is total relevant
	#k is total relevant within query
	#M is total items
	#N is total items within query
	M = len(rank.keys())
	n = len(test_group)

	ov = 0
	tt = []
	tt_look = {}
	#for N in range(1, len(pval)):
	for N in range(1, int(len(pval)*cutoff)):
		this_item = pval[N-1][0]
		if this_item in test_group:
			ov+=1
		prob = 1 - hypergeom.cdf(ov, M, n, N) + hypergeom.pmf(ov, M, n, N)
		if ov>n: continue
		tt.append((ov, M, n, N, prob))
		tt_look[N] = prob

	ttx = [a for a in tt]
	tt.sort(lambda x,y:cmp(x[4], y[4]))

	ret = {}
	ret["details"] = ttx
	ret["cutoff"] = cutoff
	ret["pval"] = tt[0][4] #lowest p-value
	ret["ov"] = tt[0][0] #overlap at lowest p-value
	ret["N"] = tt[0][3] #rank position at lowest p-value
	ret["item"] = []
	ret["all_items"] = []

	for N1 in range(1, int(len(pval)*cutoff)):
		this_item = pval[N1-1][0]
		if this_item in test_group:
			if N1 <= ret["N"]:
				ret["item"].append((N1, this_item))
		ret["all_items"].append(tt_look[N1])

	fixed_interval = [0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,\
		0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95]
	fixed_pos = [int(len(pval) * i) for i in fixed_interval]
	set_fixed_pos = set(fixed_pos)
	dict1 = dict(zip(fixed_pos, fixed_interval))

	ov = 0
	pval_fixed = {}
	for N in range(1, len(pval)):
		this_item = pval[N-1][0]
		if this_item in test_group:
			ov+=1
		if N in set_fixed_pos:
			prob = 1 - hypergeom.cdf(ov, M, n, N) + hypergeom.pmf(ov, M, n, N)
			iv = dict1[N]
			pval_fixed[iv] = prob
		if ov>n: continue
	ret["pval_at_fixed"] = pval_fixed #pvalue at fixed interval
	return ret

def do_scenario(ups_tf = [], downs_tf = [], coexp_gene = [], tfs = [], rank = {}, pval = {}, cutoff=0.1):
	#get tfs
	pval_tfs = []
	rank_tfs = {}
	for t in tfs:
		if rank.has_key(t):
			pval_tfs.append(pval[rank[t]])
	pval_tfs.sort(lambda x,y:cmp(x[1], y[1]), reverse=True)
	for i,(j,k) in enumerate(pval_tfs):
		rank_tfs[j] = i
	ret_up = {}
	ret_down = {}
	ret_coexp = {}
	#filter by those in pval
	if len(ups_tf)>0:
		ups_new = [a for a in ups_tf if rank.has_key(a)]
		set_ups = set(ups_new)
		ret_up = test(pval_tfs, rank_tfs, set_ups, cutoff=cutoff)
	if len(downs_tf)>0:
		downs_new = [a for a in downs_tf if rank.has_key(a)]
		set_downs = set(downs_new)
		ret_down = test(pval_tfs, rank_tfs, set_downs, cutoff=cutoff)
	if len(coexp_gene)>0:
		coexp_new = [a for a in coexp_gene if rank.has_key(a)]
		set_coexp = set(coexp_new)
		ret_coexp = test(pval, rank, set_coexp, cutoff=cutoff)
	return {"up": ret_up, "down": ret_down, "coexp": ret_coexp}



