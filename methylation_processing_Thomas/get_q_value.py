#!/usr/bin/python

import os
import re
import sys

def read_pval(n):
	f = open(n)
	m = []
	for l in f:
		l = l.rstrip("\n").split()
		pval = float(l[1])
		effect = float(l[4])
		m.append((l[0], pval, l[2], l[3], effect))
	f.close()
	m.sort(lambda x,y:cmp(x[1], y[1]))
	return m

if __name__=="__main__":
	pval = read_pval(sys.argv[1])

	fw = open("/tmp/tmp.pval", "w")
	for i,j,k,l,m in pval:
		fw.write(str(j) + "\n")
	fw.close()

	os.system("./qval.R /tmp/tmp.pval /tmp/tmp.qval")

	f = open("/tmp/tmp.qval")
	m = []
	for l in f:
		l = l.rstrip("\n")
		m.append(float(l))
	f.close()

	pt = zip(pval, m)
	for (a,b,c,d,e),f in pt:
		print a, b, c, d, e, f
	
		
