#!/usr/bin/python

import sys
import os
import numpy
np = numpy
def read_gene_set(n):
	f = open(n)
	p = []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		magnitude = float(ll[-1])
		p.append(magnitude)
	f.close()
	nn = np.array(p)
	uq = np.percentile(nn, 75)
	lq = np.percentile(nn, 25)
	print lq, uq

	f = open(n)
	gs_pos = set([])
	gs_neg = set([])
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		gene = ll[0]
		pval = float(ll[1])
		magnitude = float(ll[-1])
		if pval<1e-2:
			if magnitude<0:
				if "|" in gene:
					gg = gene.split("|")
					for gene in gg:
						gs_neg.add((gene, magnitude / lq))
				else:
					gs_neg.add((gene, magnitude / lq))
			elif magnitude>0:
				if "|" in gene:
					gg = gene.split("|")
					for gene in gg:
						gs_pos.add((gene, magnitude / uq))
				else:
					gs_pos.add((gene, magnitude / uq))
	f.close()
	return gs_neg, gs_pos

basal_neg, basal_pos = read_gene_set("methylation_basal_like_difference_t_test.txt")
lumA_neg, lumA_pos = read_gene_set("methylation_Luminal_A_difference_t_test.txt")
lumB_neg, lumB_pos = read_gene_set("methylation_Luminal_B_difference_t_test.txt")
her2_neg, her2_pos = read_gene_set("methylation_Her2_difference_t_test.txt")

print len(basal_neg), len(basal_pos)
print len(lumA_neg), len(lumA_pos)
print len(lumB_neg), len(lumB_pos)
print len(her2_neg), len(her2_pos)


sys.stdout.write("Hypomethylated Basal-like\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in basal_neg]) + "\n")
sys.stdout.write("Hypermethylated Basal-like\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in basal_pos]) + "\n")

sys.stdout.write("Hypomethylated Luminal A\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumA_neg]) + "\n")
sys.stdout.write("Hypermethylated Luminal A\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumA_pos]) + "\n")

sys.stdout.write("Hypomethylated Luminal B\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumB_neg]) + "\n")
sys.stdout.write("Hypermethylated Luminal B\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in lumB_pos]) + "\n")

sys.stdout.write("Hypomethylated Her2-enriched\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in her2_neg]) + "\n")
sys.stdout.write("Hypermethylated Her2-enriched\n")
sys.stdout.write(" ".join(["%s:%.3f" % (i,j) for i,j in her2_pos]) + "\n")

