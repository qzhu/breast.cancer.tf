#!/usr/bin/python
import sys
import numpy
import scipy
from scipy import stats
np = numpy

if __name__=="__main__":
	f = open("Tumor.samples.MDG")
	#f = open("Tumor.samples.2")
	#f = open("Tumor.samples")
	ph = {}
	sam = []
	for l in f:
		l = l.rstrip("\n").split("\t")
		ph.setdefault(l[2], set([]))
		ph[l[2]].add(l[0])
		sam.append(l[0])
	f.close()
	f = open("Normal.samples")
	#f = open("Normal.MDG")
	#f = open("Normal.RPL")
	sam_normal = []
	for l in f:
		l = l.rstrip("\n").split("\t")
		sam_normal.append(l[0])
	f.close()

	f = open(sys.argv[1])
	m = []
	m_sam = []
	m_map = {}
	for l in f:
		lx = l.rstrip("\n")
		lxt = l.rstrip("\n").split("/")[1]
		m.append(lx)
		m_sam.append(lxt)
		m_map[lxt] = lx
	f.close()

	#p = "Luminal A"
	p = sys.argv[2]
	tcga = ph[p]
	sx = list(set(tcga) & set(m_sam)) #these are the samples for the subtype
	num_p = len(sx) #number of samples

	gene = {}
	for ml in sx:
		f = open(m_map[ml])
		for l in f:
			l = l.rstrip("\n")
			(i,j) = l.split("\t")
			gene.setdefault(i, [])
			gene[i].append(float(j))
		f.close()

	#now do it for normal
	sx = list(set(sam_normal) & set(m_map))
	num_n = len(sx) #number of samples

	gene_normal = {}
	for ml in sx:
		f = open(m_map[ml])
		for l in f:
			l = l.rstrip("\n")
			(i,j) = l.split("\t")
			gene_normal.setdefault(i, [])
			gene_normal[i].append(float(j))
		f.close()

	all_genes = set(gene.keys()) | set(gene_normal.keys())

	for g in all_genes:
		if gene.has_key(g):
			needed = num_p - len(gene[g])
		else:
			needed = num_p
		n1 = needed
		for i in range(needed):
			gene.setdefault(g, [])
			gene[g].append(0)
		if gene_normal.has_key(g):
			needed = num_n - len(gene_normal[g])
		else:
			needed = num_n
		n2 = needed
		for i in range(needed):
			gene_normal.setdefault(g, [])
			gene_normal[g].append(0)
		if n1!=0 or n2!=0: continue
		a1 = np.log2(np.array(gene[g]))
		a2 = np.log2(np.array(gene_normal[g]))
		#a1 = np.array(gene[g])
		#a2 = np.array(gene_normal[g])
		(tstat, pval) = stats.ttest_ind(a1, a2, equal_var=False)
		#print g, pval, num_p, num_n, 2**(np.average(a1) - np.average(a2))
		print g, pval, num_p, num_n, np.average(a1) - np.average(a2)
