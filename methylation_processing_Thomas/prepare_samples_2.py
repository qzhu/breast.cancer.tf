#!/usr/bin/python
import sys
def read_methyl(n):
	gene = {}
	f = open(n)
	for l in f:
		l = l.rstrip("\n").split("\t")
		if not cg.has_key(l[0]): continue
		for g in cg[l[0]]:
			gene.setdefault(g, [])
			val = float(l[1])
			gene[g].append(val / len(cg[l[0]]))
	f.close()
	gene_val = {}
	for g in gene.keys():
		gene_val[g] = float(sum(gene[g]))
	return gene_val

cg = {}
f = open("GPL_AFFY.gene_map")
for l in f:
	l = l.rstrip("\n").split("\t")
	sym = set(l[1].split(";"))
	cg.setdefault(l[0], [])
	cg[l[0]].extend(sym)
f.close()

f = open(sys.argv[1])
m = []
m_sam = []
m_map = {}
for l in f:
	lx = l.rstrip("\n")
	lxt = l.rstrip("\n").split("/")[1]
	m.append(lx)
	m_sam.append(lxt)
	m_map[lxt] = lx
f.close()
for ml in m_sam:
	mm = read_methyl(m_map[ml])
	lxt = m_map[ml].rstrip("\n").split("/")[1]
	fw = open("methyl_sample_gene/" + lxt, "w")
	for g in mm.keys():
		fw.write("%s\t%.3f\n" % (g, mm[g]))
	fw.close()
