# DNAmeth processing

We use the dataset from Fleischer et al (Genome Biology, 2012)

Running the scripts require Python 2.7 in a Unix/Linux environment.

### Input:
patient's DNA methylation locus-specific beta-values (located in methyl_sample directory)
We provide 5 samples for testing:
```
MDG-069
MDG-091
MDG-151
MicMa014
MicMa034
```

### Step 1:
run `prepare_samples_2.py` on each patient's DNAmeth profile

the output is in `methyl_sample_gene` directory. This script summarizes locus-level CpGs into gene-based value.

### Step 2:
Do t-test using `do_t_test.py`. Iterate over all samples of interest (in this case, the basal or lumA, and the normal samples). Sample list is `all.list`.

t-test finds significantly hypo- and hyper-methylated genes.

This generates a set of t_test scores (`*_difference_t_test.txt`)

### Step 3:
Do Q-value correction to correct for multiple hypothesis testing.

This is done using the `get_q_value.py` script.

after running, the results are in the files (`*_difference_t_test.qval`).

### Step 4:
Do GORILLA enrichment to test how enriched are subtype extended signature genes (ESGs) in DNA hypo-/hyper-methylations.

This is done with the `do_gorilla.py` script.

### Step 5:
Do: `build_table_cna_methylation.py`
This will build a gene-by-DNAmeth for visualization.

### Note:
We provide the output files of each step for users to check and understand the processing. Users are welcomed to replicate these steps, but note that since only 5 samples are provided as input, Steps 2-4 would not be able to run since they would require the full dataset which is in a separate download (see GSE60185).
