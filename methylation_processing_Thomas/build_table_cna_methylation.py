#!/usr/bin/python
import sys
import re
import os
def read_gene_set(n):
	f = open(n)
	x = {}
	while True:
		l = f.readline()
		if l=="": break
		l = l.rstrip("\n")
		name = l
		x[name] = {}
		gx = f.readline().rstrip("\n").split()
		for gj in gx:
			(i,j) = gj.split(":")
			x[name][i] = "%.1f" % float(j)
	f.close()
	return x
def read_tf(n):
	f = open(n)
	m = []
	for l in f:
		l = l.rstrip("\n")
		m.append(l)
	f.close()
	return m
def read_gene_map():
	f = open("gene_entrez_symbol.txt")
	es, se = {}, {}
	for l in f:
		l = l.rstrip("\n").split("\t")
		es[l[0]] = l[1]
		se[l[1]] = l[0]
	f.close()
	return es,se

if __name__=="__main__":
	cnv = "/media/storage1/qzhu/Vessela.BC.data/Thomas/CNA/all_cna_difference_t_test_gene_full_value.txt"
	meth = "/media/storage1/qzhu/Vessela.BC.data/Thomas/all_methylation_t_test_gene_full_value.txt"

	cnv_valid = ["CNA Gained Her2-enriched", "CNA Lost Her2-enriched", \
	"CNA Gained Luminal A", "CNA Lost Luminal A", \
	"CNA Gained Luminal B", "CNA Lost Luminal B", \
	"CNA Gained Basal-like", "CNA Lost Basal-like"]
	meth_valid = ["Hypermethylated Her2-enriched", "Hypomethylated Her2-enriched", \
	"Hypermethylated Luminal A", "Hypomethylated Luminal A", \
	"Hypermethylated Luminal B", "Hypomethylated Luminal B", \
	"Hypermethylated Basal-like", "Hypomethylated Basal-like"]

	c_gene = read_gene_set(cnv)
	m_gene = read_gene_set(meth)

	#entrez-symbol, symbol-entrez
	es,se = read_gene_map()
	tf = read_tf(sys.argv[1])

	col_label = []
	col_label.extend(cnv_valid[:2])
	col_label.extend(meth_valid[:2])
	col_label.extend(cnv_valid[2:4])
	col_label.extend(meth_valid[2:4])
	col_label.extend(cnv_valid[4:6])
	col_label.extend(meth_valid[4:6])
	col_label.extend(cnv_valid[6:8])
	col_label.extend(meth_valid[6:8])

	sys.stdout.write("TF\t%s\n" % ("\t".join(col_label)))

	for t in tf:
		#if not se.has_key(t): continue
		#entrez = se[t]
		val = []
		for x in cnv_valid[:2]: 
			val.append(c_gene[x].get(t, "0"))
		for x in meth_valid[:2]: 
			val.append(m_gene[x].get(t, "0"))
		for x in cnv_valid[2:4]: 
			val.append(c_gene[x].get(t, "0"))
		for x in meth_valid[2:4]: 
			val.append(m_gene[x].get(t, "0"))
		for x in cnv_valid[4:6]: 
			val.append(c_gene[x].get(t, "0"))
		for x in meth_valid[4:6]: 
			val.append(m_gene[x].get(t, "0"))
		for x in cnv_valid[6:]: 
			val.append(c_gene[x].get(t, "0"))
		for x in meth_valid[6:]: 
			val.append(m_gene[x].get(t, "0"))
		sys.stdout.write("%s\t%s\n" % (t, "\t".join(val)))
