#!/usr/bin/python
import sys
import re
import os
import random
import numpy
np = numpy
from scipy.stats import hypergeom
import reader
rp = reader

#subtypes = ["lumA", "basal", "lumB", "her2"]
subtypes = ["lumA", "basal"]
dereg = ["cna", "meth"]

def read_pval(n, valid):
	f = open(n)
	p = []
	tp, tn = [], []
	for l in f:
		l = l.rstrip("\n")
		ll = l.split()
		magnitude = float(ll[-2])
		if magnitude > 0:
			tp.append(magnitude)
		else:
			tn.append(magnitude)
		#p.append(magnitude)
	f.close()
	nn = np.array(tp)
	nnp = np.array(tn)
	uq = np.percentile(nn, 75)
	lq = np.percentile(nnp, 25)

	f = open(n)
	m = []
	valid_set = set(valid)
	for l in f:
		l = l.rstrip("\n").split()
		if not l[0] in valid_set:
			continue
		pval = float(l[1])
		effect = float(l[4])
		qval = float(l[5])
		
		if effect < 0:
			#effect = -1.0 * effect / lq
			effect = effect / lq
			#effect = -1.0 * effect
		elif effect > 0:
			effect = effect / uq
			#effect = 1.0 * effect
	
		'''
		if qval>0.001:
		#if qval>0.1:
			m.append((l[0], 0))
		else:
			m.append((l[0], effect))
		'''
		m.append((l[0], effect))

		'''
		if qval<=0.01 and effect>1:
		#if qval<=0.001:
		#if qval<=0.01:
		#if qval<=0.001:
			effect = 1
			#effect = 1.0 * effect
		else:
			effect = 0
		'''
	f.close()
	m.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank = {}
	for i,(j,k) in enumerate(m):
		rank[j] = i
	return m, rank

def shuffle_set(pval, rank):
	t1 = [x[0] for x in pval]
	t2 = [x[1] for x in pval]
	random.shuffle(t1)
	random.shuffle(t2)
	pval_new = zip(t1, t2)
	pval_new.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank_new = {}
	for i,(j,k) in enumerate(pval_new):
		rank_new[j] = i
	return pval_new, rank_new

def get_fdr(val, set_val):
	set_val.sort(reverse=True)
	index = len(set_val)
	for i,v in enumerate(set_val):
		if v < val:
			index = i + 1
			break
	index /= float(len(set_val))
	return 1.0 - index

def fdr(ar):
	rank = []
	for i,a in enumerate(ar):
		rank.append((i, a))
	rank.sort(lambda x,y:cmp(x[1],y[1]))
	trank = [0 for i in range(len(rank))]
	for ind,(i,a) in enumerate(rank):
		b_val = float(a) * float(len(rank)) / float(ind + 1)
		trank[i] = b_val
	return trank

def hocheberg_fdr(pval):
	#subtypes = ["lumA", "basal"]
	#dereg = ["cna", "meth"]
	for s in subtypes:
		tval_up = []
		tval_down = []
		tval_coexp = []
		for d in dereg:
			tval_up.append(pval[s][d]["up"])
			tval_down.append(pval[s][d]["down"])
			tval_coexp.append(pval[s][d]["coexp"])
		fdr_up = fdr(tval_up)
		fdr_down = fdr(tval_down)
		fdr_coexp = fdr(tval_coexp)
		i = 0
		for d in dereg:
			pval[s][d]["up"] = fdr_up[i]
			pval[s][d]["down"] = fdr_down[i]
			pval[s][d]["coexp"] = fdr_coexp[i]
			i+=1
	return pval

if __name__=="__main__":
	valid_genes = rp.read_genes("gene_entrez_symbol.txt", sep="\t", field=1)
	tfs = rp.read_tfs("human.TF.list.in.Seek.txt")

	param1 = {"her2":"erbb2", "lumA":"lumA", "lumB":"lumB.NEW", "basal":"basal"}
	param2 = {"her2":"Her2", "lumA":"luminal_A", "lumB":"luminal_B", "basal":"basal_like"}
	param3 = {"her2":"her2", "lumA":"lumA", "lumB":"lumB", "basal":"basal"}

	path = {"cna":"cna.TCGA/cna_", "meth":"methyl.TCGA/methylation_"}

	pvals = {}
	cutoff = 0.5
	for s in subtypes:
		#all coexpressed genes
		#coexp = rp.read_genes("/home/qzhu/enrich/coexpressed/%s" % s, sep=" ", field=0)

		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.all.exp.0.05" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/motifs.good/%s" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/results.integrated.%s" % s, sep=" ", field=0)
		#targets
		coexp = rp.read_genes("seq.3/result.%s.0.10" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/analyzed.prob.3/result.%s.0.10.txt" % s, sep=" ", field=0)
		#motif-derived coexpressed TFs
		#downs = rp.read_tfs("/home/qzhu/enrich/downstream/%s" % s)

		#just coexpressed TFs
		#downs = rp.read_tfs("/home/qzhu/enrich/coexpressed/tf/%s" % s)

		#downs_nonspecific = rp.read_tfs("/home/qzhu/enrich/downstream/%s.nonspecific" % s)
		#downs = set(downs) | set(downs_nonspecific)
		#chip-seq derived TFs
		#ups = rp.read_tfs("/home/qzhu/enrich/upstream/%s" % s)
		#ups = set(ups) - set(downs)
		
		combined = rp.read_tfs("combined/%s" % s)
		pvals.setdefault(s, {})

		for d in dereg:
			test_file = path[d] + param2[s] + "_difference.t.test.qval"
			if d=="meth" and s=="her2":
				test_file = "methyl.TCGA/short_methylation_" + param2[s] + "_difference.t.test.qval"
			pval, rank = read_pval(test_file, valid_genes)
			ret = rp.do_scenario(ups_tf=combined, downs_tf=combined, coexp_gene=coexp, tfs=tfs, \
				rank=rank, pval=pval, cutoff=cutoff)
			pvals[s].setdefault(d, {})
			pvals[s][d]["up"] = ret["up"]["pval"]
			pvals[s][d]["down"] = ret["down"]["pval"]
			pvals[s][d]["coexp"] = ret["coexp"]["pval"]
			#print s, d, ret["up"]["pval"], ret["down"]["pval"], ret["coexp"]["pval"]
	#pvals = hocheberg_fdr(pvals)
	for s in subtypes:
		for d in dereg:
			sys.stdout.write("%s\t%s\t%.2e\t%.2e\t%.2e\n" % (s, d, pvals[s][d]["up"], pvals[s][d]["down"], pvals[s][d]["coexp"]))
