# Supplementary website for the breast cancer TF paper

Here you will find the some supplementary data and scripts for the paper. 

### Processed data

*  TCGA DNAmeth, BRCA cohort, breast cancer subtype-specific deregulations for individual subtype (summarized to genes, derived from t-test) (Directory: `methyl.TCGA`)
*  TCGA Copy Number Aberrations, BRCA cohort, breast cancer subtype-specific CNA gains and losses for individual subtype (summarized to genes, derived from t-test) (Directory: `cna.TCGA`)
*  Metabric Copy Number Aberrations, subtype-specific CNA gains and losses (same as above) (Directory: `cna.Metabric`)
*  Fleischer et al (Genome Biology 2012), DNAmeth, breast cancer subtype-specific DNAmeth (same as above) (Directory: `methyl.Thomas`)

### TF Enrichment

For each data type, we test the statistical associations of breast cancer subtype-related TFs and their targets accumulating more DNAmeth, CNA deregulations than at random genes.

This is performed by the GORILLA a rank-based enrichment test:

*  The script `do_gorilla_meth_aug4.py` performs DNAmeth association test in Fleischer et al dataset.
*  The script `do_gorilla_tcga_aug4.py` performs DNAmeth, CNA association tests in TCGA dataset.
*  The script `do_gorilla_curtis_aug4.py` performs CNA association test in Metabric dataset.

### Extended Signature Genes and TF list

Extended signature genes (ESG) are inferred by SEEK (Zhu et al 2015) based on expanding a small set of seed genes corresponding to known subtype markers. Expansion is based on coexpression. See directory `coexpressed/*` for ESGs.

Inferred TF regulators of ESGs per subtype in this paper are located in the directory `combined/lumA` and `combined/basal`.

### Processing of CNA dataset and DNAmeth dataset

We preprocess the CNA and DNAmeth raw datasets, and illustrate with a small subset of CNA samples (from Perou lab) and DNAmeth samples (from Fleischer et al).

Directory `cna_processing` contains the data and scripts. Refer to `cna_processing/README.md` for details about running.

Directory `methylation_processing_Thomas` contains the data and scripts. Refer to `methylation_processing_Thomas/README.md` for details about running.

