FOXA1 1311.9723788 P300_t47d:199.38 Gata3_mcf7:75.09 Znf217_mcf7:157.40 Cmyc_mcf7:102.96 Eralpha_t47d:144.90 Sin3a_mcf7:183.50 Tead4_mcf7:87.13 Foxa1_t47d:199.38 Max_mcf7:162.22
ESR1 1016.34149521 P300_t47d:24.63 Gata3_mcf7:170.83 Znf217_mcf7:199.38 Cmyc_mcf7:70.05 Eralpha_t47d:94.25 Sin3a_mcf7:174.53 Tead4_mcf7:63.15 Foxa1_t47d:82.16 Max_mcf7:137.36
SEMA3C 1000.21565896 P300_t47d:105.22 Gata3_mcf7:76.95 Znf217_mcf7:87.90 Cmyc_mcf7:44.52 Eralpha_t47d:117.76 Sin3a_mcf7:199.38 Tead4_mcf7:199.38 Foxa1_t47d:55.87 Max_mcf7:113.23
EVL 970.996011679 P300_t47d:72.22 Gata3_mcf7:102.63 Znf217_mcf7:110.10 Cmyc_mcf7:121.60 Eralpha_t47d:53.12 Sin3a_mcf7:142.41 Tead4_mcf7:83.41 Foxa1_t47d:86.12 Max_mcf7:199.38
KRT18 955.174815598 P300_t47d:77.54 Gata3_mcf7:72.36 Znf217_mcf7:145.59 Cmyc_mcf7:106.96 Eralpha_t47d:82.20 Sin3a_mcf7:169.74 Tead4_mcf7:112.38 Foxa1_t47d:57.35 Max_mcf7:131.05
PREX1 942.512050252 P300_t47d:93.63 Gata3_mcf7:199.38 Znf217_mcf7:26.06 Cmyc_mcf7:199.38 Eralpha_t47d:92.75 Sin3a_mcf7:56.04 Tead4_mcf7:33.48 Foxa1_t47d:105.62 Max_mcf7:136.17
RHOB 869.3839551 P300_t47d:100.41 Gata3_mcf7:39.92 Znf217_mcf7:110.75 Cmyc_mcf7:72.06 Eralpha_t47d:199.38 Sin3a_mcf7:124.25 Tead4_mcf7:30.12 Foxa1_t47d:117.81 Max_mcf7:74.68
FAM174B 851.387597524 P300_t47d:97.99 Gata3_mcf7:44.28 Znf217_mcf7:32.39 Cmyc_mcf7:100.31 Eralpha_t47d:73.80 Sin3a_mcf7:155.08 Tead4_mcf7:68.82 Foxa1_t47d:117.27 Max_mcf7:161.45
KCTD3 702.24367202 P300_t47d:94.15 Gata3_mcf7:49.78 Znf217_mcf7:44.20 Cmyc_mcf7:37.39 Eralpha_t47d:73.92 Sin3a_mcf7:126.69 Tead4_mcf7:111.49 Foxa1_t47d:104.94 Max_mcf7:59.69
CXXC5 686.712248852 P300_t47d:33.67 Gata3_mcf7:45.78 Znf217_mcf7:65.06 Cmyc_mcf7:127.85 Eralpha_t47d:125.28 Sin3a_mcf7:86.69 Tead4_mcf7:26.89 Foxa1_t47d:34.11 Max_mcf7:141.38
VAV3 642.2337362 P300_t47d:90.12 Gata3_mcf7:61.53 Znf217_mcf7:121.27 Cmyc_mcf7:36.83 Eralpha_t47d:41.78 Sin3a_mcf7:93.93 Tead4_mcf7:62.32 Foxa1_t47d:101.37 Max_mcf7:33.09
MED13L 625.094920161 P300_t47d:130.78 Gata3_mcf7:13.84 Eralpha_t47d:245.00 Sin3a_mcf7:66.09 Foxa1_t47d:151.69 Max_mcf7:17.69
MYOF 620.6039454 P300_t47d:82.39 Gata3_mcf7:51.24 Znf217_mcf7:54.42 Cmyc_mcf7:41.01 Eralpha_t47d:47.19 Sin3a_mcf7:148.04 Tead4_mcf7:55.53 Foxa1_t47d:54.81 Max_mcf7:85.97
LYPD6B 609.887517268 P300_t47d:71.77 Gata3_mcf7:77.62 Znf217_mcf7:67.39 Cmyc_mcf7:22.63 Eralpha_t47d:52.48 Sin3a_mcf7:100.53 Tead4_mcf7:83.25 Foxa1_t47d:67.36 Max_mcf7:66.86
PKIB 603.66746103 P300_t47d:18.13 Gata3_mcf7:100.02 Znf217_mcf7:121.42 Cmyc_mcf7:49.66 Eralpha_t47d:34.38 Sin3a_mcf7:139.50 Tead4_mcf7:55.31 Foxa1_t47d:18.24 Max_mcf7:67.00
AGR3 566.991940662 P300_t47d:53.35 Gata3_mcf7:31.00 Znf217_mcf7:89.79 Cmyc_mcf7:6.16 Eralpha_t47d:163.23 Sin3a_mcf7:61.95 Tead4_mcf7:45.74 Foxa1_t47d:106.14 Max_mcf7:9.62
FAM102A 560.255069525 P300_t47d:63.45 Gata3_mcf7:38.56 Znf217_mcf7:44.65 Cmyc_mcf7:79.71 Eralpha_t47d:77.96 Sin3a_mcf7:96.07 Tead4_mcf7:47.47 Foxa1_t47d:41.36 Max_mcf7:71.02
RASEF 554.123292288 P300_t47d:39.51 Gata3_mcf7:63.65 Znf217_mcf7:111.64 Cmyc_mcf7:11.24 Eralpha_t47d:37.67 Sin3a_mcf7:125.56 Tead4_mcf7:56.53 Foxa1_t47d:36.17 Max_mcf7:72.17
SYTL2 502.656978796 P300_t47d:16.13 Gata3_mcf7:52.54 Znf217_mcf7:78.56 Cmyc_mcf7:28.70 Sin3a_mcf7:155.88 Tead4_mcf7:75.22 Foxa1_t47d:27.41 Max_mcf7:68.21
RERG 484.276664727 P300_t47d:37.31 Gata3_mcf7:13.06 Znf217_mcf7:54.82 Cmyc_mcf7:23.96 Eralpha_t47d:150.10 Sin3a_mcf7:59.63 Tead4_mcf7:54.93 Foxa1_t47d:50.98 Max_mcf7:39.49
PBX1 482.837967385 P300_t47d:133.19 Gata3_mcf7:12.96 Znf217_mcf7:21.14 Cmyc_mcf7:11.36 Eralpha_t47d:88.06 Sin3a_mcf7:52.47 Tead4_mcf7:26.53 Foxa1_t47d:114.75 Max_mcf7:22.37
DEGS2 480.354570123 P300_t47d:17.82 Gata3_mcf7:36.40 Cmyc_mcf7:107.19 Eralpha_t47d:75.43 Sin3a_mcf7:52.03 Foxa1_t47d:38.07 Max_mcf7:153.43
ECE1 470.867557746 P300_t47d:12.43 Gata3_mcf7:55.03 Znf217_mcf7:67.54 Cmyc_mcf7:56.59 Eralpha_t47d:83.16 Sin3a_mcf7:71.82 Tead4_mcf7:53.13 Foxa1_t47d:22.84 Max_mcf7:48.33
PLEKHF2 446.419566021 P300_t47d:53.68 Gata3_mcf7:29.19 Znf217_mcf7:74.15 Cmyc_mcf7:40.39 Eralpha_t47d:46.85 Sin3a_mcf7:83.14 Tead4_mcf7:18.88 Foxa1_t47d:61.73 Max_mcf7:38.41
MREG 444.661262066 P300_t47d:25.43 Gata3_mcf7:51.23 Znf217_mcf7:44.68 Cmyc_mcf7:34.82 Eralpha_t47d:39.24 Sin3a_mcf7:90.32 Tead4_mcf7:27.50 Foxa1_t47d:52.61 Max_mcf7:78.84
MYB 437.344102447 P300_t47d:22.29 Gata3_mcf7:37.21 Znf217_mcf7:23.97 Cmyc_mcf7:10.70 Eralpha_t47d:75.90 Sin3a_mcf7:103.57 Tead4_mcf7:79.99 Foxa1_t47d:30.37 Max_mcf7:53.34
PTP4A2 433.296222945 P300_t47d:60.00 Gata3_mcf7:12.35 Znf217_mcf7:21.62 Cmyc_mcf7:43.17 Eralpha_t47d:136.07 Sin3a_mcf7:68.15 Foxa1_t47d:68.27 Max_mcf7:23.67
SLC40A1 427.216960598 P300_t47d:47.25 Gata3_mcf7:51.67 Znf217_mcf7:111.48 Cmyc_mcf7:10.67 Eralpha_t47d:57.89 Sin3a_mcf7:52.13 Tead4_mcf7:25.41 Foxa1_t47d:49.29 Max_mcf7:21.41
ABLIM3 420.970057458 P300_t47d:36.51 Gata3_mcf7:28.01 Znf217_mcf7:88.10 Cmyc_mcf7:25.41 Eralpha_t47d:34.36 Sin3a_mcf7:69.72 Tead4_mcf7:31.50 Foxa1_t47d:47.31 Max_mcf7:60.05
SYBU 413.334159638 P300_t47d:70.54 Gata3_mcf7:35.02 Znf217_mcf7:42.26 Cmyc_mcf7:46.13 Eralpha_t47d:79.62 Tead4_mcf7:27.88 Foxa1_t47d:89.00 Max_mcf7:22.90
VEZF1 412.371711715 P300_t47d:39.96 Gata3_mcf7:9.57 Znf217_mcf7:19.98 Cmyc_mcf7:54.93 Eralpha_t47d:51.79 Sin3a_mcf7:86.32 Tead4_mcf7:38.63 Foxa1_t47d:30.09 Max_mcf7:81.10
XBP1 411.036741914 P300_t47d:33.34 Gata3_mcf7:45.05 Znf217_mcf7:66.91 Cmyc_mcf7:41.82 Eralpha_t47d:14.14 Sin3a_mcf7:67.58 Tead4_mcf7:45.20 Foxa1_t47d:32.95 Max_mcf7:64.05
MLPH 410.786481348 P300_t47d:17.09 Gata3_mcf7:32.98 Znf217_mcf7:64.53 Cmyc_mcf7:35.65 Eralpha_t47d:31.25 Sin3a_mcf7:85.05 Tead4_mcf7:27.56 Foxa1_t47d:21.41 Max_mcf7:95.27
ARSG 409.940150318 P300_t47d:16.26 Gata3_mcf7:41.82 Znf217_mcf7:51.47 Cmyc_mcf7:47.50 Eralpha_t47d:34.60 Sin3a_mcf7:86.22 Tead4_mcf7:49.29 Foxa1_t47d:32.27 Max_mcf7:50.50
FOXP1 409.403667675 P300_t47d:22.21 Gata3_mcf7:41.43 Znf217_mcf7:44.43 Cmyc_mcf7:17.59 Eralpha_t47d:19.45 Sin3a_mcf7:102.79 Tead4_mcf7:52.94 Foxa1_t47d:81.81 Max_mcf7:26.76
FAM214A 396.960912843 P300_t47d:39.64 Gata3_mcf7:36.95 Znf217_mcf7:22.63 Cmyc_mcf7:21.97 Eralpha_t47d:36.42 Sin3a_mcf7:106.22 Tead4_mcf7:25.64 Foxa1_t47d:83.20 Max_mcf7:24.29
MCCC2 392.013877508 P300_t47d:23.56 Gata3_mcf7:50.21 Znf217_mcf7:66.58 Cmyc_mcf7:12.03 Eralpha_t47d:54.05 Sin3a_mcf7:56.67 Tead4_mcf7:52.97 Foxa1_t47d:34.88 Max_mcf7:41.06
TOB1 385.630639612 P300_t47d:44.64 Gata3_mcf7:5.80 Znf217_mcf7:35.47 Cmyc_mcf7:29.76 Eralpha_t47d:72.79 Sin3a_mcf7:60.18 Tead4_mcf7:36.65 Foxa1_t47d:42.96 Max_mcf7:57.38
AGR2 384.701223872 P300_t47d:31.48 Gata3_mcf7:46.20 Znf217_mcf7:100.18 Cmyc_mcf7:6.16 Eralpha_t47d:51.40 Sin3a_mcf7:55.26 Tead4_mcf7:17.00 Foxa1_t47d:67.40 Max_mcf7:9.62
SLC16A6 381.216496973 P300_t47d:16.26 Gata3_mcf7:34.13 Znf217_mcf7:44.55 Cmyc_mcf7:47.50 Eralpha_t47d:28.56 Sin3a_mcf7:86.22 Tead4_mcf7:49.29 Foxa1_t47d:24.19 Max_mcf7:50.50
STC2 379.965557281 P300_t47d:11.03 Gata3_mcf7:13.36 Znf217_mcf7:10.47 Cmyc_mcf7:68.40 Eralpha_t47d:29.70 Sin3a_mcf7:91.27 Tead4_mcf7:12.68 Foxa1_t47d:8.42 Max_mcf7:134.64
C1orf21 361.581763788 Gata3_mcf7:29.62 Znf217_mcf7:90.39 Cmyc_mcf7:11.51 Eralpha_t47d:31.87 Sin3a_mcf7:87.35 Tead4_mcf7:79.25 Foxa1_t47d:31.58
MYO5C 361.555919799 P300_t47d:41.18 Gata3_mcf7:15.39 Znf217_mcf7:55.65 Cmyc_mcf7:64.39 Sin3a_mcf7:56.51 Tead4_mcf7:16.10 Foxa1_t47d:30.39 Max_mcf7:81.95
KIF13B 356.175378745 P300_t47d:36.50 Gata3_mcf7:25.48 Znf217_mcf7:42.79 Eralpha_t47d:75.71 Sin3a_mcf7:56.02 Tead4_mcf7:28.36 Foxa1_t47d:54.35 Max_mcf7:36.97
AHNAK 356.129629803 P300_t47d:34.30 Gata3_mcf7:22.06 Znf217_mcf7:23.97 Cmyc_mcf7:50.01 Eralpha_t47d:42.98 Sin3a_mcf7:53.97 Tead4_mcf7:30.70 Foxa1_t47d:57.84 Max_mcf7:40.31
LRRFIP1 353.754108139 P300_t47d:37.33 Gata3_mcf7:12.21 Cmyc_mcf7:79.24 Sin3a_mcf7:39.36 Tead4_mcf7:28.26 Foxa1_t47d:68.29 Max_mcf7:89.05
GPD1L 351.945589731 P300_t47d:12.80 Gata3_mcf7:39.47 Znf217_mcf7:66.63 Cmyc_mcf7:23.37 Eralpha_t47d:21.58 Sin3a_mcf7:38.61 Tead4_mcf7:31.56 Foxa1_t47d:45.06 Max_mcf7:72.87
ADCY9 349.136092197 P300_t47d:21.41 Gata3_mcf7:42.01 Znf217_mcf7:68.24 Cmyc_mcf7:47.46 Eralpha_t47d:54.99 Sin3a_mcf7:39.86 Tead4_mcf7:27.50 Foxa1_t47d:8.42 Max_mcf7:39.24
SLC19A2 348.723824307 P300_t47d:17.29 Gata3_mcf7:64.62 Znf217_mcf7:71.50 Cmyc_mcf7:13.16 Eralpha_t47d:28.11 Sin3a_mcf7:55.54 Tead4_mcf7:34.86 Foxa1_t47d:15.59 Max_mcf7:48.06
THSD4 342.147703329 P300_t47d:18.80 Gata3_mcf7:30.14 Znf217_mcf7:24.19 Cmyc_mcf7:28.54 Eralpha_t47d:84.73 Sin3a_mcf7:46.56 Tead4_mcf7:31.24 Foxa1_t47d:57.66 Max_mcf7:20.27
GATA3 339.694300952 P300_t47d:28.74 Gata3_mcf7:12.84 Znf217_mcf7:22.10 Cmyc_mcf7:50.41 Eralpha_t47d:10.36 Sin3a_mcf7:76.54 Tead4_mcf7:39.53 Foxa1_t47d:34.12 Max_mcf7:65.06
PIP 335.748825622 P300_t47d:59.29 Gata3_mcf7:11.23 Eralpha_t47d:185.93 Foxa1_t47d:79.31
TBC1D9 329.547781928 P300_t47d:24.13 Znf217_mcf7:63.60 Cmyc_mcf7:22.32 Eralpha_t47d:36.30 Sin3a_mcf7:53.68 Tead4_mcf7:59.70 Foxa1_t47d:33.48 Max_mcf7:36.33
CCND1 329.089902999 P300_t47d:27.09 Znf217_mcf7:6.79 Cmyc_mcf7:68.15 Eralpha_t47d:25.25 Sin3a_mcf7:60.80 Tead4_mcf7:27.82 Foxa1_t47d:16.28 Max_mcf7:96.91
SCCPDH 326.560349745 P300_t47d:17.82 Gata3_mcf7:25.67 Znf217_mcf7:41.78 Cmyc_mcf7:5.83 Sin3a_mcf7:100.69 Tead4_mcf7:55.47 Foxa1_t47d:32.03 Max_mcf7:47.28
CPEB4 326.272964591 P300_t47d:23.15 Gata3_mcf7:45.59 Znf217_mcf7:20.54 Cmyc_mcf7:22.86 Eralpha_t47d:35.44 Sin3a_mcf7:85.58 Foxa1_t47d:34.47 Max_mcf7:58.66
COX6C 325.483489232 Gata3_mcf7:24.36 Cmyc_mcf7:24.89 Eralpha_t47d:16.22 Sin3a_mcf7:83.62 Tead4_mcf7:93.94 Foxa1_t47d:24.54 Max_mcf7:57.91
BHLHE40 325.241755457 P300_t47d:23.17 Gata3_mcf7:12.72 Znf217_mcf7:21.97 Cmyc_mcf7:52.57 Eralpha_t47d:25.10 Sin3a_mcf7:61.07 Tead4_mcf7:53.81 Foxa1_t47d:17.75 Max_mcf7:57.08
TP53INP1 321.486419981 P300_t47d:18.53 Gata3_mcf7:32.43 Znf217_mcf7:24.32 Cmyc_mcf7:34.11 Sin3a_mcf7:72.14 Tead4_mcf7:13.67 Foxa1_t47d:30.38 Max_mcf7:95.91
ABAT 315.630367126 P300_t47d:10.95 Gata3_mcf7:13.04 Znf217_mcf7:45.06 Cmyc_mcf7:28.42 Eralpha_t47d:53.29 Sin3a_mcf7:52.55 Tead4_mcf7:58.10 Foxa1_t47d:16.06 Max_mcf7:38.16
RABEP1 315.34332756 P300_t47d:55.80 Gata3_mcf7:5.95 Znf217_mcf7:10.42 Cmyc_mcf7:11.22 Eralpha_t47d:67.78 Sin3a_mcf7:47.85 Tead4_mcf7:38.67 Foxa1_t47d:40.80 Max_mcf7:36.86
CAPN8 313.499060797 P300_t47d:23.34 Gata3_mcf7:14.64 Znf217_mcf7:34.18 Cmyc_mcf7:29.84 Eralpha_t47d:34.81 Sin3a_mcf7:54.97 Tead4_mcf7:28.36 Foxa1_t47d:37.70 Max_mcf7:55.64
KCNK15 310.7889099 P300_t47d:15.80 Gata3_mcf7:16.74 Znf217_mcf7:29.44 Cmyc_mcf7:25.64 Eralpha_t47d:43.90 Sin3a_mcf7:33.71 Tead4_mcf7:26.86 Foxa1_t47d:24.46 Max_mcf7:94.24
C17orf58 310.509013708 P300_t47d:17.44 Gata3_mcf7:12.42 Znf217_mcf7:43.09 Cmyc_mcf7:59.51 Sin3a_mcf7:70.45 Tead4_mcf7:30.76 Foxa1_t47d:34.61 Max_mcf7:42.23
MTUS1 308.215559114 P300_t47d:11.32 Gata3_mcf7:47.97 Znf217_mcf7:20.38 Cmyc_mcf7:22.09 Eralpha_t47d:56.37 Sin3a_mcf7:71.11 Foxa1_t47d:60.23 Max_mcf7:18.74
ITGB5 305.560647956 P300_t47d:23.08 Gata3_mcf7:28.43 Znf217_mcf7:47.63 Cmyc_mcf7:18.74 Eralpha_t47d:18.33 Sin3a_mcf7:57.15 Tead4_mcf7:31.72 Foxa1_t47d:50.93 Max_mcf7:29.55
LIMA1 304.317972206 P300_t47d:42.14 Gata3_mcf7:10.86 Znf217_mcf7:18.64 Cmyc_mcf7:22.16 Eralpha_t47d:19.41 Sin3a_mcf7:63.29 Tead4_mcf7:22.98 Foxa1_t47d:56.14 Max_mcf7:48.70
SPOPL 301.340516732 Gata3_mcf7:26.22 Znf217_mcf7:65.27 Cmyc_mcf7:45.62 Eralpha_t47d:31.93 Sin3a_mcf7:52.84 Foxa1_t47d:39.36 Max_mcf7:40.10
KIAA1324 297.44409313 P300_t47d:8.60 Gata3_mcf7:31.40 Znf217_mcf7:41.85 Cmyc_mcf7:33.77 Eralpha_t47d:24.33 Sin3a_mcf7:71.88 Tead4_mcf7:6.34 Foxa1_t47d:28.28 Max_mcf7:51.00
UGCG 296.123513542 P300_t47d:19.52 Gata3_mcf7:17.35 Znf217_mcf7:57.72 Cmyc_mcf7:29.58 Eralpha_t47d:10.34 Sin3a_mcf7:43.27 Tead4_mcf7:38.47 Foxa1_t47d:34.21 Max_mcf7:45.66
WWP1 289.864470882 P300_t47d:10.92 Gata3_mcf7:27.44 Znf217_mcf7:46.65 Cmyc_mcf7:23.27 Eralpha_t47d:33.07 Sin3a_mcf7:86.61 Foxa1_t47d:24.27 Max_mcf7:37.64
TLE3 288.525170147 P300_t47d:24.40 Gata3_mcf7:21.55 Znf217_mcf7:23.97 Cmyc_mcf7:17.99 Eralpha_t47d:16.70 Sin3a_mcf7:56.71 Tead4_mcf7:30.06 Foxa1_t47d:38.73 Max_mcf7:58.43
RBM47 284.976912695 P300_t47d:27.80 Gata3_mcf7:30.06 Znf217_mcf7:22.00 Cmyc_mcf7:10.52 Sin3a_mcf7:64.58 Tead4_mcf7:13.03 Foxa1_t47d:45.83 Max_mcf7:71.16
C16orf45 284.539029543 Gata3_mcf7:48.76 Znf217_mcf7:62.31 Cmyc_mcf7:34.18 Sin3a_mcf7:50.11 Tead4_mcf7:67.51 Foxa1_t47d:3.80 Max_mcf7:17.87
MEGF9 284.264473945 P300_t47d:23.32 Gata3_mcf7:24.12 Znf217_mcf7:44.55 Cmyc_mcf7:11.40 Eralpha_t47d:16.76 Sin3a_mcf7:36.96 Tead4_mcf7:51.12 Foxa1_t47d:56.85 Max_mcf7:19.17
LOC145837 283.426391327 P300_t47d:24.64 Gata3_mcf7:31.26 Znf217_mcf7:42.48 Cmyc_mcf7:11.69 Eralpha_t47d:18.44 Sin3a_mcf7:75.25 Tead4_mcf7:12.61 Foxa1_t47d:38.55 Max_mcf7:28.50
SIAH2 279.479437181 P300_t47d:14.20 Gata3_mcf7:7.04 Znf217_mcf7:21.70 Cmyc_mcf7:34.88 Eralpha_t47d:50.07 Sin3a_mcf7:39.58 Tead4_mcf7:19.56 Foxa1_t47d:31.44 Max_mcf7:61.01
KDM4B 277.578889312 P300_t47d:27.82 Gata3_mcf7:25.88 Znf217_mcf7:10.19 Eralpha_t47d:112.30 Sin3a_mcf7:27.01 Tead4_mcf7:25.73 Foxa1_t47d:38.90 Max_mcf7:9.73
PRSS23 275.439549735 Gata3_mcf7:25.17 Znf217_mcf7:71.12 Cmyc_mcf7:11.39 Sin3a_mcf7:85.88 Tead4_mcf7:26.86 Foxa1_t47d:16.36 Max_mcf7:38.66
SPDEF 274.386031265 P300_t47d:25.47 Gata3_mcf7:5.90 Znf217_mcf7:10.90 Cmyc_mcf7:42.99 Eralpha_t47d:60.15 Sin3a_mcf7:9.06 Foxa1_t47d:43.46 Max_mcf7:76.46
PDZK1 272.310419317 P300_t47d:10.81 Gata3_mcf7:33.13 Znf217_mcf7:45.21 Cmyc_mcf7:17.47 Eralpha_t47d:10.27 Sin3a_mcf7:50.25 Tead4_mcf7:43.08 Foxa1_t47d:12.73 Max_mcf7:49.36
REEP5 267.916193778 P300_t47d:49.96 Gata3_mcf7:12.33 Znf217_mcf7:10.85 Cmyc_mcf7:23.07 Eralpha_t47d:26.39 Sin3a_mcf7:58.93 Foxa1_t47d:57.58 Max_mcf7:28.82
MKL2 265.15688683 P300_t47d:23.86 Gata3_mcf7:13.41 Znf217_mcf7:43.32 Cmyc_mcf7:6.17 Eralpha_t47d:48.14 Sin3a_mcf7:44.81 Tead4_mcf7:56.65 Foxa1_t47d:18.54 Max_mcf7:10.26
WBP1L 259.002067376 P300_t47d:28.32 Gata3_mcf7:3.88 Znf217_mcf7:7.22 Cmyc_mcf7:25.48 Eralpha_t47d:97.33 Sin3a_mcf7:32.96 Tead4_mcf7:9.22 Foxa1_t47d:30.94 Max_mcf7:23.65
SLC7A8 256.807457369 P300_t47d:5.76 Gata3_mcf7:33.81 Znf217_mcf7:37.79 Cmyc_mcf7:26.29 Eralpha_t47d:32.86 Sin3a_mcf7:50.35 Foxa1_t47d:37.66 Max_mcf7:32.29
KIF16B 254.167189845 Gata3_mcf7:11.65 Znf217_mcf7:42.28 Cmyc_mcf7:11.46 Eralpha_t47d:34.79 Sin3a_mcf7:69.34 Tead4_mcf7:26.18 Foxa1_t47d:40.14 Max_mcf7:18.33
TRIM8 251.408843042 P300_t47d:22.87 Gata3_mcf7:11.90 Cmyc_mcf7:36.36 Eralpha_t47d:17.17 Sin3a_mcf7:51.32 Foxa1_t47d:45.52 Max_mcf7:66.27
CA12 247.81287054 P300_t47d:11.59 Gata3_mcf7:32.82 Znf217_mcf7:43.82 Cmyc_mcf7:11.75 Eralpha_t47d:47.62 Sin3a_mcf7:35.53 Tead4_mcf7:42.67 Foxa1_t47d:13.06 Max_mcf7:8.96
SLC2A10 245.393529585 P300_t47d:3.70 Znf217_mcf7:28.68 Cmyc_mcf7:69.80 Sin3a_mcf7:22.11 Tead4_mcf7:26.69 Foxa1_t47d:21.33 Max_mcf7:73.08
WFS1 245.375851682 P300_t47d:11.70 Gata3_mcf7:25.46 Znf217_mcf7:43.01 Cmyc_mcf7:46.40 Eralpha_t47d:17.15 Sin3a_mcf7:35.93 Foxa1_t47d:47.98 Max_mcf7:17.73
ERGIC1 241.125349794 P300_t47d:25.84 Gata3_mcf7:23.28 Cmyc_mcf7:34.71 Eralpha_t47d:34.30 Sin3a_mcf7:49.53 Foxa1_t47d:35.26 Max_mcf7:38.21
BLVRA 236.198826737 P300_t47d:11.41 Gata3_mcf7:14.82 Znf217_mcf7:54.62 Cmyc_mcf7:34.63 Eralpha_t47d:29.48 Sin3a_mcf7:33.73 Foxa1_t47d:29.21 Max_mcf7:28.29
CLSTN2 234.939726813 P300_t47d:24.63 Gata3_mcf7:38.11 Znf217_mcf7:44.02 Eralpha_t47d:16.05 Sin3a_mcf7:51.02 Tead4_mcf7:26.47 Foxa1_t47d:34.63
APPL2 232.689516337 P300_t47d:24.37 Gata3_mcf7:48.09 Eralpha_t47d:18.29 Sin3a_mcf7:64.80 Tead4_mcf7:25.83 Foxa1_t47d:31.48 Max_mcf7:19.84
LONRF2 231.823905255 Gata3_mcf7:29.17 Znf217_mcf7:46.40 Cmyc_mcf7:59.58 Sin3a_mcf7:51.77 Foxa1_t47d:7.69 Max_mcf7:37.23
GALNT7 229.358275312 Gata3_mcf7:14.80 Znf217_mcf7:65.77 Cmyc_mcf7:23.16 Sin3a_mcf7:55.74 Tead4_mcf7:32.04 Max_mcf7:37.84
AFF3 228.855665773 P300_t47d:34.19 Gata3_mcf7:26.13 Znf217_mcf7:21.29 Eralpha_t47d:54.09 Sin3a_mcf7:54.85 Foxa1_t47d:38.30
ICA1 227.251115795 P300_t47d:16.48 Gata3_mcf7:19.76 Znf217_mcf7:44.86 Cmyc_mcf7:18.51 Sin3a_mcf7:29.60 Tead4_mcf7:28.78 Foxa1_t47d:30.65 Max_mcf7:38.62
P2RX4 224.280662638 P300_t47d:12.42 Gata3_mcf7:12.56 Znf217_mcf7:34.94 Cmyc_mcf7:28.99 Eralpha_t47d:34.94 Sin3a_mcf7:27.98 Tead4_mcf7:26.44 Foxa1_t47d:17.85 Max_mcf7:28.15
CCNG2 223.344380048 P300_t47d:68.96 Gata3_mcf7:11.58 Znf217_mcf7:20.51 Cmyc_mcf7:11.94 Eralpha_t47d:16.20 Sin3a_mcf7:20.60 Foxa1_t47d:53.41 Max_mcf7:20.16
DLG5 222.013906916 P300_t47d:23.32 Gata3_mcf7:17.90 Znf217_mcf7:11.02 Cmyc_mcf7:35.47 Eralpha_t47d:26.63 Sin3a_mcf7:36.41 Tead4_mcf7:14.40 Foxa1_t47d:18.85 Max_mcf7:38.00
KIAA0040 221.949101117 P300_t47d:34.45 Gata3_mcf7:11.23 Cmyc_mcf7:21.50 Eralpha_t47d:51.53 Foxa1_t47d:103.24
SUMF1 219.095168263 Gata3_mcf7:13.84 Znf217_mcf7:45.41 Cmyc_mcf7:21.14 Eralpha_t47d:25.29 Sin3a_mcf7:48.68 Tead4_mcf7:29.64 Foxa1_t47d:2.52 Max_mcf7:32.56
ARHGAP32 216.30076551 P300_t47d:48.23 Gata3_mcf7:11.43 Znf217_mcf7:62.36 Sin3a_mcf7:33.49 Foxa1_t47d:60.79
DHCR24 211.848414921 P300_t47d:29.64 Gata3_mcf7:23.05 Znf217_mcf7:21.04 Cmyc_mcf7:21.99 Eralpha_t47d:15.54 Sin3a_mcf7:32.90 Tead4_mcf7:12.77 Foxa1_t47d:54.91
CELSR1 209.594358167 P300_t47d:22.97 Gata3_mcf7:11.36 Cmyc_mcf7:45.20 Eralpha_t47d:59.29 Sin3a_mcf7:15.90 Foxa1_t47d:18.05 Max_mcf7:36.84
MRFAP1L1 208.443893303 P300_t47d:17.88 Gata3_mcf7:5.77 Znf217_mcf7:20.94 Cmyc_mcf7:39.50 Eralpha_t47d:21.40 Sin3a_mcf7:18.54 Tead4_mcf7:20.68 Foxa1_t47d:18.02 Max_mcf7:45.70
COG7 207.694699669 Gata3_mcf7:11.24 Cmyc_mcf7:28.43 Sin3a_mcf7:69.96 Tead4_mcf7:26.47 Foxa1_t47d:17.34 Max_mcf7:54.25
FAM134B 207.385061847 Gata3_mcf7:25.24 Znf217_mcf7:20.86 Cmyc_mcf7:23.27 Eralpha_t47d:36.62 Sin3a_mcf7:37.95 Tead4_mcf7:13.32 Foxa1_t47d:24.12 Max_mcf7:26.01
MSX2 204.304344663 P300_t47d:27.27 Gata3_mcf7:24.18 Znf217_mcf7:31.41 Cmyc_mcf7:10.70 Sin3a_mcf7:42.00 Foxa1_t47d:50.78 Max_mcf7:17.96
CMBL 203.672895743 Gata3_mcf7:3.93 Znf217_mcf7:7.47 Cmyc_mcf7:48.74 Sin3a_mcf7:47.79 Foxa1_t47d:12.57 Max_mcf7:83.18
FBP1 203.394085198 P300_t47d:17.64 Gata3_mcf7:18.89 Znf217_mcf7:22.40 Cmyc_mcf7:29.14 Eralpha_t47d:35.63 Sin3a_mcf7:27.30 Foxa1_t47d:12.36 Max_mcf7:40.04
KIAA0232 200.675418924 P300_t47d:29.57 Znf217_mcf7:27.88 Cmyc_mcf7:39.34 Eralpha_t47d:55.16 Sin3a_mcf7:13.62 Tead4_mcf7:8.71 Foxa1_t47d:14.79 Max_mcf7:11.61
CRIP1 199.908804011 P300_t47d:30.96 Gata3_mcf7:3.45 Cmyc_mcf7:47.76 Eralpha_t47d:3.61 Sin3a_mcf7:36.49 Foxa1_t47d:9.01 Max_mcf7:68.62
MAST4 193.910210179 P300_t47d:5.67 Gata3_mcf7:11.42 Znf217_mcf7:22.78 Cmyc_mcf7:5.63 Eralpha_t47d:26.50 Sin3a_mcf7:60.42 Foxa1_t47d:32.91 Max_mcf7:28.59
APBB2 191.404256569 P300_t47d:21.80 Gata3_mcf7:12.84 Znf217_mcf7:20.99 Cmyc_mcf7:22.72 Eralpha_t47d:15.62 Sin3a_mcf7:38.94 Foxa1_t47d:27.85 Max_mcf7:30.64
TFF1 191.125050776 P300_t47d:3.63 Gata3_mcf7:23.14 Znf217_mcf7:23.79 Cmyc_mcf7:35.22 Eralpha_t47d:18.50 Sin3a_mcf7:27.82 Tead4_mcf7:19.59 Foxa1_t47d:6.83 Max_mcf7:32.61
SIDT1 189.321771378 P300_t47d:5.98 Gata3_mcf7:18.24 Znf217_mcf7:18.72 Cmyc_mcf7:8.93 Eralpha_t47d:15.71 Sin3a_mcf7:39.92 Tead4_mcf7:13.57 Foxa1_t47d:23.10 Max_mcf7:45.15
LRIG1 188.770363187 Gata3_mcf7:11.84 Znf217_mcf7:20.79 Eralpha_t47d:52.24 Sin3a_mcf7:32.84 Foxa1_t47d:33.63 Max_mcf7:37.43
KLHDC2 184.343256126 P300_t47d:33.32 Cmyc_mcf7:12.31 Sin3a_mcf7:40.23 Tead4_mcf7:26.66 Foxa1_t47d:31.74 Max_mcf7:40.08
TMEM63C 182.894283753 Gata3_mcf7:16.43 Cmyc_mcf7:27.98 Eralpha_t47d:5.46 Sin3a_mcf7:50.68 Foxa1_t47d:27.94 Max_mcf7:54.41
HHAT 182.846436803 P300_t47d:23.83 Cmyc_mcf7:10.85 Eralpha_t47d:39.41 Sin3a_mcf7:18.46 Foxa1_t47d:72.10 Max_mcf7:18.19
PRR15L 180.838190464 P300_t47d:15.87 Gata3_mcf7:2.98 Znf217_mcf7:10.52 Cmyc_mcf7:38.54 Eralpha_t47d:3.89 Sin3a_mcf7:37.06 Tead4_mcf7:12.90 Foxa1_t47d:15.00 Max_mcf7:44.08
EXOC6 178.522055683 P300_t47d:11.81 Gata3_mcf7:11.23 Znf217_mcf7:23.03 Cmyc_mcf7:11.61 Eralpha_t47d:18.22 Sin3a_mcf7:20.42 Tead4_mcf7:54.06 Foxa1_t47d:28.14
MGRN1 178.342510442 Znf217_mcf7:10.19 Cmyc_mcf7:47.79 Eralpha_t47d:12.96 Sin3a_mcf7:25.40 Tead4_mcf7:13.86 Foxa1_t47d:16.11 Max_mcf7:52.03
TSC22D3 176.663795133 Znf217_mcf7:20.38 Cmyc_mcf7:24.04 Eralpha_t47d:15.88 Sin3a_mcf7:87.33 Foxa1_t47d:17.92 Max_mcf7:11.11
TTC39A 175.017464011 P300_t47d:18.82 Gata3_mcf7:5.73 Cmyc_mcf7:24.60 Eralpha_t47d:55.06 Sin3a_mcf7:17.01 Foxa1_t47d:15.14 Max_mcf7:38.66
IFT43 174.048782728 P300_t47d:11.91 Gata3_mcf7:6.89 Cmyc_mcf7:23.49 Sin3a_mcf7:35.53 Tead4_mcf7:14.98 Foxa1_t47d:17.06 Max_mcf7:64.19
TGFB3 174.048782728 P300_t47d:11.91 Gata3_mcf7:6.89 Cmyc_mcf7:23.49 Sin3a_mcf7:35.53 Tead4_mcf7:14.98 Foxa1_t47d:17.06 Max_mcf7:64.19
GPR160 173.73550102 P300_t47d:27.03 Znf217_mcf7:20.46 Cmyc_mcf7:23.98 Eralpha_t47d:5.50 Foxa1_t47d:37.16 Max_mcf7:59.60
SLC39A6 173.387405903 Gata3_mcf7:15.63 Znf217_mcf7:22.10 Cmyc_mcf7:13.24 Eralpha_t47d:5.72 Sin3a_mcf7:43.80 Tead4_mcf7:40.52 Foxa1_t47d:11.24 Max_mcf7:21.14
ELP2 173.387405903 Gata3_mcf7:15.63 Znf217_mcf7:22.10 Cmyc_mcf7:13.24 Eralpha_t47d:5.72 Sin3a_mcf7:43.80 Tead4_mcf7:40.52 Foxa1_t47d:11.24 Max_mcf7:21.14
NUCB2 173.336882751 P300_t47d:12.50 Gata3_mcf7:11.55 Cmyc_mcf7:23.83 Eralpha_t47d:31.87 Sin3a_mcf7:51.67 Foxa1_t47d:18.50 Max_mcf7:23.42
C5orf15 172.649038888 P300_t47d:6.58 Gata3_mcf7:5.83 Znf217_mcf7:10.14 Cmyc_mcf7:39.89 Eralpha_t47d:8.70 Sin3a_mcf7:33.21 Tead4_mcf7:13.03 Foxa1_t47d:17.91 Max_mcf7:37.36
IGF1R 172.351097731 Gata3_mcf7:6.11 Cmyc_mcf7:28.48 Eralpha_t47d:8.66 Sin3a_mcf7:42.10 Tead4_mcf7:14.34 Foxa1_t47d:42.72 Max_mcf7:29.94
IFT140 170.812781594 P300_t47d:8.13 Gata3_mcf7:8.88 Znf217_mcf7:6.86 Cmyc_mcf7:37.62 Sin3a_mcf7:42.18 Foxa1_t47d:13.86 Max_mcf7:53.27
STARD10 169.439328148 P300_t47d:6.52 Gata3_mcf7:7.30 Znf217_mcf7:5.63 Cmyc_mcf7:32.67 Eralpha_t47d:14.53 Sin3a_mcf7:28.47 Tead4_mcf7:7.01 Foxa1_t47d:7.24 Max_mcf7:60.08
C9orf152 168.351252608 P300_t47d:36.36 Cmyc_mcf7:23.49 Eralpha_t47d:25.01 Sin3a_mcf7:8.23 Foxa1_t47d:55.01 Max_mcf7:20.25
