#!/usr/bin/python
import sys
import re
import os
import numpy
np = numpy
from scipy.stats import hypergeom
sys.path.insert(0, "/home/qzhu/enrich/SNP3")
import reader
rp = reader

subtypes = ["lumA", "basal", "norm", "lumB", "her2"]
dereg = ["ssm"]

def read_pval(n, valid, subtype):
	f = open(n)
	mm = {}
	while True:
		l = f.readline()
		if l=="": break
		l = l.rstrip("\n")
		tit = l
		sub = tit.split(" ", 2)[-1]
		genes = f.readline().rstrip("\n").split()
		if sub!=subtype:
			continue
		for t in genes:
			g1,g2 = t.split(":")
			mm.setdefault(g1, [])
			mm[g1].append(float(g2))
	f.close()
	m = []
	for k in mm.keys():
		m.append((k, max(mm[k])))
	m.sort(lambda x, y:cmp(x[1], y[1]), reverse=True)
	rank = {}
	for i,(j,k) in enumerate(m):
		rank[j] = i

	return m, rank

def get_fdr(val, set_val):
	set_val.sort(reverse=True)
	index = len(set_val)
	for i,v in enumerate(set_val):
		if v < val:
			index = i + 1
			break
	index /= float(len(set_val))
	return 1.0 - index

def fdr(ar):
	rank = []
	for i,a in enumerate(ar):
		rank.append((i, a))
	rank.sort(lambda x,y:cmp(x[1],y[1]))
	trank = [0 for i in range(len(rank))]
	for ind,(i,a) in enumerate(rank):
		b_val = float(a) * float(len(rank)) / float(ind + 1)
		trank[i] = b_val
	return trank

def hocheberg_fdr(pval):
	#subtypes = ["lumA", "basal"]
	#dereg = ["cna", "meth"]
	for s in subtypes:
		tval_up = []
		tval_down = []
		tval_coexp = []
		for d in dereg:
			tval_up.append(pval[s][d]["up"])
			tval_down.append(pval[s][d]["down"])
			tval_coexp.append(pval[s][d]["coexp"])
		fdr_up = fdr(tval_up)
		fdr_down = fdr(tval_down)
		fdr_coexp = fdr(tval_coexp)
		i = 0
		for d in dereg:
			pval[s][d]["up"] = fdr_up[i]
			pval[s][d]["down"] = fdr_down[i]
			pval[s][d]["coexp"] = fdr_coexp[i]
			i+=1
	return pval

if __name__=="__main__":
	
	valid_genes = rp.read_genes("/home/qzhu/enrich/gene_entrez_symbol.txt", sep="\t", field=1)
	tfs = rp.read_tfs("/home/qzhu/enrich/human.TF.list.in.Seek.txt")

	cc2={"norm":"Normal-like", "her2":"HER2-enriched", "lumA":"Luminal A", "basal":"Basal-like", "lumB":"Luminal B"}

	path={"ssm":"/home/qzhu/enrich/ssm.TCGA/all_subtypes_deleterious_ssm.txt"}
	pvals = {}
	cutoff = 0.5
	for s in subtypes:	
		#coexp = rp.read_genes("/home/qzhu/ENCODE/motifs.good/%s" % s, sep=" ", field=0)
		coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.0.10" % s, sep=" ", field=0)
		#coexp = rp.read_genes("/home/qzhu/ENCODE/seq.3/result.%s.all.exp.0.10" % s, sep=" ", field=0)
		downs = rp.read_tfs("/home/qzhu/enrich/downstream/%s" % s)
		#downs_nonspecific = rp.read_tfs("/home/qzhu/enrich/downstream/%s.nonspecific" % s)
		#downs = set(downs) | set(downs_nonspecific)
		ups = rp.read_tfs("/home/qzhu/enrich/upstream/%s" % s)
		#ups = set(ups) - set(downs)
		pvals.setdefault(s, {})
		for d in dereg:
			pval, rank = read_pval(path[d], valid_genes, cc2[s])
			ret = rp.do_scenario(ups_tf=ups, downs_tf=downs, coexp_gene=coexp, tfs=tfs, \
				rank=rank, pval=pval, cutoff=cutoff)
			pvals[s].setdefault(d, {})
			pvals[s][d]["up"] = ret["up"]["pval"]
			pvals[s][d]["down"] = ret["down"]["pval"]
			pvals[s][d]["coexp"] = ret["coexp"]["pval"]
			#print s, d, ret["up"]["pval"], ret["down"]["pval"], ret["coexp"]["pval"]

	pvals = hocheberg_fdr(pvals)
	for s in subtypes:
		for d in dereg:
			sys.stdout.write("%s\t%s\t%.2e\t%.2e\t%.2e\n" % (s, d, pvals[s][d]["up"], pvals[s][d]["down"], pvals[s][d]["coexp"]))
